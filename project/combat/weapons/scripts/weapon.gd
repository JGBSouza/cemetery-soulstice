@tool
class_name Weapon
extends Node2D
## This script handles the weapon functionality for the player.
## It's behavior and looks change when weapon_data changes.

@export var weapon_data: WeaponData

var in_attack: bool = false

# Called every frame
func _process(delta):
	# Continuously update the visuals of the weapon based on the weapon description
	update_weapon_visual()
	
	# If we are in play mode:
	if not Engine.is_editor_hint():
		# If running in game mode, make the weapon look at the mouse position.
		if not in_attack:
			var direction := self.global_position.direction_to(self.get_global_mouse_position())
			self.global_rotation = lerp_angle(self.global_rotation, direction.angle(), delta * 5)
#			self.look_at(get_global_mouse_position())
			self.position.y = sin((Time.get_ticks_msec()) * delta * 0.02) * 10
			if get_global_mouse_position().x - self.global_position.x < 0:
				self.scale.x = -1
				self.rotation_degrees -= 180
			else:
				self.scale.x = 1

func _physics_process(_delta):
	# If we are in play mode:
	if not Engine.is_editor_hint():
		if not in_attack and Input.is_action_just_pressed("attack"):
			in_attack = true
			var tween := self.get_tree().create_tween()
			
			# Wind-up animation
			tween.tween_property($Pivot, "rotation_degrees", -weapon_data.angle_range * 0.5, 0.05 / weapon_data.base_speed)
			tween.parallel().tween_property($Pivot/Sprite, "rotation_degrees", -135 if weapon_data.flip_v else -45, 0.05 / weapon_data.base_speed)
			tween.tween_callback(func(): 
				await self.get_tree().create_timer(0.07 / weapon_data.base_speed).timeout
				$Pivot/Hitbox.visible = true
			)
			# Attack animation
			tween.tween_property($Pivot, "rotation_degrees", weapon_data.angle_range * 0.5, 0.2 / weapon_data.base_speed).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN)
			tween.parallel().tween_property($Pivot/Sprite, "rotation_degrees", 45 if weapon_data.flip_v else 135, 0.2 / weapon_data.base_speed).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_OUT)
			tween.tween_callback(func(): $Pivot/Hitbox.visible = false)
			# Return animation
			tween.tween_property($Pivot, "rotation_degrees", 0.0, 0.3 / weapon_data.base_speed)
			tween.parallel().tween_property($Pivot/Sprite, "rotation_degrees", -45 if weapon_data.flip_v else 45, 0.3 / weapon_data.base_speed)
			
			tween.tween_callback(func(): in_attack = false)

# Updates the visuals of the weapon. This method should be called
# whenever you want changes to weapon_data to take effect.
func update_weapon_visual():
	$Pivot/Sprite.texture = weapon_data.atlas
	$Pivot/Sprite.position = weapon_data.sprite_offset
	$Pivot/Sprite.rotation_degrees = 45
	$Pivot/Sprite.flip_v = weapon_data.flip_v
	$Pivot/Sprite.rotation_degrees = -45 if weapon_data.flip_v else 45

	$Pivot/Sprite.region_enabled = true
	$Pivot/Sprite.region_rect = Rect2(weapon_data.sprite_size * weapon_data.sprite_index, weapon_data.sprite_size)

	$Pivot.scale = Vector2(weapon_data.scale, weapon_data.scale)

	$Pivot/Hitbox/Shape.shape.size = weapon_data.hitbox_size
	$Pivot/Hitbox.position = weapon_data.hitbox_offset

# Returns the base damage of the weapon
func get_damage() -> float:
	return weapon_data.base_damage

# Gets the weapon data resource of the currently equiped weapon
func get_weapon_data() -> WeaponData:
	return weapon_data

# Sets a new weapon as the currently equiped weapon
func change_weapon(weapon_data_: WeaponData) -> void:
	weapon_data = weapon_data_
	update_weapon_visual()
