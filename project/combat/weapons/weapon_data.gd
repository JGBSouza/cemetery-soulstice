class_name WeaponData
extends Resource

## This is a Resource class the has all the data related to
## different weapon types in the game.
## The weapon entity attatched to the player reacts accordingly
## depending on which WeaponData is attatched to it.

@export var prefix: String
@export var name: String
@export var level: int

@export var atlas: Texture
@export var sprite_size: Vector2
@export var sprite_index: Vector2
@export var sprite_offset: Vector2
@export var scale: float = 2.0
@export var flip_v: bool = false

@export var hitbox_size: Vector2
@export var hitbox_offset: Vector2

@export var angle_range: float = 90.0
@export var base_speed: float = 1.0
@export var base_damage: float = 1.0
