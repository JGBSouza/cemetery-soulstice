class_name AttackStrategy
# Abstract class Strategy that implements mobs attack strategy 

extends Node

var _attacker : Mob
	
func attack() -> void:
	pass

func _init(attacker: Mob):
	_attacker = attacker


	
