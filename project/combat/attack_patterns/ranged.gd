class_name RangedAttack
extends AttackStrategy

var _shoot_direction : Vector2
var _projectile : Projectile

# Stops mob and await casting time to shot projectile 
func attack() -> void:
	_attacker.velocity = Vector2.ZERO
	_projectile = _attacker.get_projectile()
	await _attacker.get_tree().create_timer(_attacker.get_casting_time()).timeout
	_shoot_projectile()

# Calculates projectile offset from mob's body
func calculate_projectile_offset(_player_direction) -> Vector2:
	if( _player_direction.x >= 0 ):
		return _projectile.get_offset()
	return Vector2(-1, 0) * _projectile.get_offset()

# Calculates position and instanciate projectile
func _shoot_projectile() -> void:
	var _projectile_instance = _projectile.clone()
	var target_position = _attacker.get_target_position()
	var attacker_position = _attacker.get_current_position()
	var player_direction = (target_position - attacker_position).normalized()
	
	var _shoot_position = _attacker.get_position() +\
							calculate_projectile_offset(player_direction)
	_projectile_instance.set_projectile_position(_shoot_position)
	
	_shoot_direction  = (target_position - _shoot_position).normalized()
	_projectile_instance.set_direction(_shoot_direction)
	_attacker.get_parent().add_child(_projectile_instance)
