class_name MeleeAttack
extends AttackStrategy

# Stops player to realize attack
func attack()-> void:
	_attacker.velocity = Vector2.ZERO
