class_name DashAttack
# Implements mobs dash attack strategy 

extends AttackStrategy

# Calculates and set mob attack velocity 
func attack() -> void:
	var attack_direction = calculates_attack_direction()
	var attack_speed = _attacker.get_dash_speed()
	_attacker.velocity = attack_direction * attack_speed

# Calculate attack direction based on player and mob position
func calculates_attack_direction() -> Vector2:
	var current_position = _attacker.get_current_position()
	var target_position = _attacker.get_target_position()
	return (target_position - current_position).normalized()
