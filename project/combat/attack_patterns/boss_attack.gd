class_name BossAttack
# Implements boss attack strategy 

extends AttackStrategy

# Attack strategy related variables
var _currentAttackStrategy : int = 0
var _attackStrategys : Dictionary
var _attack_switch_timer : Timer

# Initialize super class and attack strategys
func _init(attacker : Mob):
	super._init(attacker)
	_attackStrategys[0] = MeleeAttack.new(_attacker)
	_attackStrategys[1] = DashAttack.new(_attacker)
	
# Switch current attack strategy
func _switch_strategy() -> void:
	_currentAttackStrategy = 1 - _currentAttackStrategy
	_attack_switch_timer.start()

func _ready() -> void:
	_setup_timer()
	_attack_switch_timer.start()  

# Setup switcher timer to alternate between attack strategys
func _setup_timer():
	_attack_switch_timer = Timer.new()
	_attack_switch_timer.wait_time = _attacker.get_attack_switcher_cooldown()
	_attack_switch_timer.connect("timeout", _switch_strategy)
	_attacker.add_child(_attack_switch_timer)

# Call current attack strategy attack method
func attack() -> void:
	_attackStrategys[_currentAttackStrategy].attack()
