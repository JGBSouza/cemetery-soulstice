class_name FireSkullProjectile
# Basic class for FireSkull's projectile

extends Projectile

# Projectile related packed scene	
var _scene : PackedScene
 
# Initialize super and load own projectile scene
func _init():
	super._init()
	_scene = preload("res://combat/projectiles/scenes/fire_skull_projectile.tscn")
	_offset = Vector2(57, 17)
	
# Define clone method that create new instances of scene that owns this script
func clone() -> FireSkullProjectile:
	return _scene.instantiate()
