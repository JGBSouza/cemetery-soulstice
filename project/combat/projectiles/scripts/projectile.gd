class_name Projectile
# Basic Projectile class that extends Hitbox class 
extends Hitbox

# Projectile related variables
@export var _speed : float
var _offset : Vector2
var _direction: Vector2 = Vector2.ZERO
var _animation : AnimationPlayer

# Called when the object is ready
func _ready():
	_animation = $AnimationPlayer

# Projectile Setters
func set_direction(direction) -> void:
	_direction = direction
	
func set_projectile_position(pos: Vector2) -> void:
	position = pos

# Projectile Getters
func get_offset() -> Vector2:
	return _offset

# Process projectile movimentation and animation
func _process(_delta) -> void:
	position += _speed * _direction 
	_animation.play("shoot")

# Abstract method clone to create new projectile instance
func clone():
	pass

# Called when the animation ends to destroy the projectile
func _on_animation_player_animation_finished(anim_name) -> void:
	if anim_name == "shoot":
		queue_free()
