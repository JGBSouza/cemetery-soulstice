class_name StinkflyProjectile
# Basic class for StinkflyProjectile's projectile

extends Projectile

var _scene : PackedScene

func _init():
	super._init()
	_scene = preload("res://combat/projectiles/scenes/stinkfly_projectile.tscn")
	_offset = Vector2(57, 17)
	
# Define clone method that create new instances of scene that owns this script
func clone() -> StinkflyProjectile:
	return _scene.instantiate()
