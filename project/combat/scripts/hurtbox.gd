class_name Hurtbox
extends Area2D

## This script implements a basic Hurtbox
## The collision layer 2 is the layer related to
## hitboxes and hurtboxes.

func _init():
	self.collision_layer = 0
	self.collision_mask = 2

func _ready() -> void:
	connect("area_entered", _on_area_entered)

func _on_area_entered(hitbox: Hitbox) -> void:
	if hitbox == null or friendly_fire(hitbox):
		return
	var damage = 20
	owner.take_damage(damage)

func friendly_fire(hitbox : Hitbox) -> bool:
	return 	owner is Mob and hitbox is Projectile or \
	 		owner is Player and hitbox.get_parent().get_parent() is Weapon or\
			owner is Mob and hitbox.get_parent().get_parent() is Mob
