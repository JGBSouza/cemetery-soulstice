class_name Hitbox
extends Area2D

## This script implements a basic Hitbox.
## The collision layer 2 is the layer related to
## hitboxes and hurtboxes.

func _init():
	self.collision_layer = 2
	self.collision_mask = 0
