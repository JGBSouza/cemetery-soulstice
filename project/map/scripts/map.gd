extends TileMap
## Generates and handles all map logic and data.

# Layers constants as enum
enum Layers { TERRAIN, OBJECTS, BORDERS }
# TileSetSources constants as enum
enum Sources { 
	GRASS_TERRAIN,
	STONE_TERRAIN,
	GRAVESTONES,
	OTHER_OBJECTS,
	PLANTS,
	STRUCTURES,
	BORDERS,
}
# Procedural map size NxN
const MAP_SIZE: int = 100
# Size of map tile
const TILE_SIZE = Vector2i(32, 32)
# Map proportion related constants
const _GRAVESTONES_AREA: float = 0.4 # 
const _OBJECTS_PROBABILITY: float = 0.15
# Noise textures used to decide which terrain/object to place in a map cell
var _terrain_noise := FastNoiseLite.new()
var _objects_noise := FastNoiseLite.new()
# Array used to place random structures lastly on the map -- maybe no longer necessary
var _structures_positions: Array[Vector2i]

func get_map_size() -> int:
	return MAP_SIZE

func get_map_center() -> Vector2:
	return map_to_local(Vector2i(MAP_SIZE / 2, MAP_SIZE / 2))

func get_tile_set_size() -> Vector2i:
	return TILE_SIZE

func get_structures_positions() -> Array[Vector2i]:
	return _structures_positions
# Called when the node enters the tree
func _ready():
	# Initializes the random seed
	randomize()
	_terrain_noise.set_seed(randi())
	_objects_noise.set_seed(randi())
	
	# Iterates over the cells, deciding what to put in each cell. 
	# Iterates outside the map too, so it can put grass on it.
	for i in range(-MAP_SIZE / 5, MAP_SIZE * 1.2):
		for j in range(-MAP_SIZE / 5, MAP_SIZE * 1.2):
			var cell := Vector2i(i, j)
			# Terrain is required in every cell and is placed in the bottom layer
			if _on_outside_area(cell):
				# Outside area can only have grass terrain
				_put_grass(cell)
				continue
			else:
				# Inside area can have stone terrain too
				_put_random_terrain(cell)
			# Uses the random float function to decide whether an object will be placed
			if not _on_empty_area(cell) and randf() <= _OBJECTS_PROBABILITY:
				if _on_gravestone_area(cell):
						_put_gravestone(cell)
				else:
					_put_random_thing(cell)
	
	# Places, at last, the random structures on the map -- maybe no longer necessary
	for cell in _structures_positions:
		_put_structure(cell)
	
	# Places the stone walls on the borders
	_put_borders()

# Places a grass terrain in the given cell
func _put_grass(cell: Vector2i):
	_add_source_random_tile(cell, Layers.TERRAIN, Sources.GRASS_TERRAIN)

# Decides (based on random noise texture) which terrain to place in a given cell (and places it)
func _put_random_terrain(cell: Vector2i, _force_source = null) -> void:
	const MULT: int = 10
	const THRESHOLD: float = -0.2
	if _terrain_noise.get_noise_2d(cell.x * MULT, cell.y * MULT) >= THRESHOLD:
		_add_source_random_tile(cell, Layers.TERRAIN, Sources.GRASS_TERRAIN)
	else:
		_add_source_random_tile(cell, Layers.TERRAIN, Sources.STONE_TERRAIN)

# Decides (based on random noise texture) which object to place in a given cell
func _put_random_thing(cell: Vector2i) -> void:
	const MULT: int = 10
	const STRUCTURE_THRESHOLD: float = -0.5
	const PLANTS_THRESHOLD: float = 0.4
	
	var noise_result: float = _objects_noise.get_noise_2d(cell.x * MULT, cell.y * MULT)
	
	if noise_result <= STRUCTURE_THRESHOLD:
		_structures_positions.append(cell)
	elif noise_result <= PLANTS_THRESHOLD:
		_put_plant(cell)
	else:
		_put_other(cell)

# Places a random structure in the given cell
func _put_structure(cell: Vector2i) -> void:
	_add_source_random_scene(cell, Layers.OBJECTS, Sources.STRUCTURES)

# Places a random gravestone in the given cell
func _put_gravestone(cell: Vector2i) -> void:
	_add_source_random_scene(cell, Layers.OBJECTS, Sources.GRAVESTONES)

# Places a random plant in the given cell
func _put_plant(cell: Vector2i) -> void:
	_add_source_random_scene(cell, Layers.OBJECTS, Sources.PLANTS)

# Places a random miscellaneous object in the given cell
func _put_other(cell: Vector2i) -> void:
	_add_source_random_scene(cell, Layers.OBJECTS, Sources.OTHER_OBJECTS)

# Places, in the given cell at the given layer, a random tile from the specified source (= group of tiles)
func _add_source_random_tile(cell: Vector2i, layer: int, source_id: int) -> void:
	var source: TileSetSource = tile_set.get_source(source_id)
	var random_tile_idx: int = randi() % source.get_tiles_count()
	var random_tile: Vector2i = source.get_tile_id(random_tile_idx)
	set_cell(layer, cell, source_id, random_tile)

# Places, in the given cell at the given layer, a random scene from the specified source (= group of scenes)
func _add_source_random_scene(cell: Vector2i, layer: int, source_id: int) -> void:
	var source: TileSetScenesCollectionSource = tile_set.get_source(source_id)
	var random_scene_idx: int = randi() % source.get_scene_tiles_count()
	var random_scene: int = source.get_scene_tile_id(random_scene_idx)
	set_cell(layer, cell, source_id, Vector2i(0, 0), random_scene)

# Returns if a given cell is inside the gravestone area (calculated as a percentage of 
# the center of the map)
func _on_gravestone_area(cell: Vector2i) -> bool:
	var half_map_size = MAP_SIZE / 2
	if (
		cell.x >= half_map_size - half_map_size * _GRAVESTONES_AREA
		and cell.x <= half_map_size + half_map_size * _GRAVESTONES_AREA
		and cell.y >= half_map_size - half_map_size * _GRAVESTONES_AREA
		and cell.y <= half_map_size + half_map_size * _GRAVESTONES_AREA
	):
		return  true
	else:
		return false

# Returns if a given cell is outside the game map
func _on_outside_area(cell: Vector2i) -> bool:
	if cell.x <= 1 or cell.x >= MAP_SIZE - 1 or cell.y <=1 or cell.y >= MAP_SIZE - 1:
		return true
	else:
		return false

# Returns if a given cell is inside the "empty area" in the center of the map. 
# There should be no objects in it.
func _on_empty_area(cell: Vector2i) -> bool:
	var half_map_size = MAP_SIZE / 2
	if (
		cell.x >= half_map_size - half_map_size * _GRAVESTONES_AREA / 3
		and cell.x <= half_map_size + half_map_size * _GRAVESTONES_AREA / 3
		and cell.y >= half_map_size - half_map_size * _GRAVESTONES_AREA / 3
 		and cell.y <= half_map_size + half_map_size * _GRAVESTONES_AREA / 3
	):
		return  true
	else:
		return false

func _put_borders() -> void:
	var corners := [
		Vector2i(-2, -2), 
		Vector2i(MAP_SIZE + 1, -2), 
		Vector2i(-2, MAP_SIZE + 1),
		Vector2i(MAP_SIZE + 1, MAP_SIZE + 1),
	]
	# Put corner walls
	for cell in corners:
		set_cell(Layers.BORDERS, cell, Sources.BORDERS, Vector2i(1, 1))
	
	# Put top and bottom walls
	for x in range(MAP_SIZE):
		set_cell(Layers.BORDERS, Vector2i(x, -2), Sources.BORDERS, Vector2i(5, 1))
		set_cell(Layers.BORDERS, Vector2i(x, MAP_SIZE + 1), Sources.BORDERS, Vector2i(5, 1))
	
	# Puts left and right walls
	for y in range(-2, MAP_SIZE):
		set_cell(Layers.BORDERS, Vector2i(-1, y), Sources.BORDERS, Vector2i(7, 1))
		set_cell(Layers.BORDERS, Vector2i(MAP_SIZE, y + 1), Sources.BORDERS, Vector2i(7, 1))

