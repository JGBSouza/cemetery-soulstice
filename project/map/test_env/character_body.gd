extends CharacterBody2D


func _ready():
	self.position = get_parent().get_node("TileMap").map_to_local(Vector2i(50,50))


func _physics_process(_delta):
	var direction: Vector2
	direction = Vector2(Input.get_axis("ui_left", "ui_right"), Input.get_axis("ui_up", "ui_down"))
	if direction:
		velocity = direction * 300
	
	move_and_slide()
