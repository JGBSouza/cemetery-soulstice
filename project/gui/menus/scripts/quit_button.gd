extends Button
# Generic button script. Used when the button exits the game.

func _on_pressed():
	get_tree().quit()
