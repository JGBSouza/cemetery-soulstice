extends Interface
## Weapons menu interface code.

const COST_PER_LEVEL: int = 3

var _player: Player

@onready var random_slots: Array[WeaponCard] = [
	$CenterContainer/VBoxContainer/Options/Card,
	$CenterContainer/VBoxContainer/Options/Card2,
	$CenterContainer/VBoxContainer/Options/Card3,
]
@onready var upgrade_slot: WeaponCard = $CenterContainer/VBoxContainer/Options/UpgradeWeapon/Card4
@onready var upgrade_section: Control = $CenterContainer/VBoxContainer/Options/UpgradeWeapon

# Called when the object is created 
func _ready():
	# Try to get player reference
	if get_tree().has_group("player"):
		_player = get_tree().get_nodes_in_group("player")[0]
	# Connect all weapon buttons related signals
	for slot in random_slots:
		slot.choosen_card.connect(choose_card)
	upgrade_slot.choosen_card.connect(choose_card)

func display():
	set_menu()
	self.set_process(true)
	self.show()

func close():
	interface_closed.emit()
	self.set_process(false)
	self.hide()

# Updates the menu info and set all the weapon options
func set_menu():
	$CenterContainer/VBoxContainer/SoulsCounter/Counter.text = str(_player.get_current_souls())
	
	var weapon_data_handler := WeaponDataHandler.new()
	var current_weapon: WeaponData = _player.get_current_weapon()
	
	# Set upgrade weapon button
	var upgrade: WeaponData = weapon_data_handler.get_upgrade(current_weapon)
	# If the upgrade weapon does exist
	if upgrade:
		set_slot(upgrade_slot, weapon_data_handler.get_upgrade(current_weapon))
		upgrade_section.visible = true
	else:
		upgrade_section.visible = false
	
	# Set all other random weapon buttons
	var random_weapons: Array[WeaponData] = \
		weapon_data_handler.get_random_weapons(3, current_weapon.level, [current_weapon])
	for i in range(random_slots.size()):
		set_slot(random_slots[i], random_weapons[i])

# Set a weapon button to represent the given Weapon Data
func set_slot(slot: WeaponCard, weapon_data: WeaponData):
	var cost: int = weapon_data.level * COST_PER_LEVEL
	slot.set_weapon(weapon_data, cost)
	if cost > _player.get_current_souls():
		slot.disable()
	else:
		slot.enable()

# Choose a weapon and close the menu
func choose_card(card: WeaponCard):
	_player.change_weapon(card.weapon_data)
	_player.change_souls(-card.cost)
	close()
