extends Interface
## Pause menu interface code.

func display():
	self.show()

func close():
	interface_closed.emit()
	self.hide()

func _unhandled_input(event):
	if event.is_action_pressed("ui_cancel"):
		unpause()

func unpause():
	get_tree().paused = false
	close()
