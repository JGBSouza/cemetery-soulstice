extends Button
# Generic button script. Used when the button activates a scene change.

@export_file("*.tscn") var next_scene_path


func _on_pressed():
	# Scene transition singleton
	Transition.transition(next_scene_path)
