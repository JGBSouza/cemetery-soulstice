class_name WeaponDataHandler
extends Node
## Handles WeaponData related requests.

const MAX_LEVEL: int = 6

# Returns the weapon upgrade of the given weapon (in WeaponData)
func get_upgrade(weapon_data: WeaponData) -> WeaponData:
	# Check if the upgrade exists
	if weapon_data.level + 1 > MAX_LEVEL:
		return null
	else:
		return get_weapon_data(weapon_data.name.to_lower(), weapon_data.level + 1)

# Given a name and level, returns the corresponding WeaponData
func get_weapon_data(res_name: String, level: int):
	return load("res://combat/weapons/weapons_data/" + res_name + "/" + res_name + str(level) + ".tres")

# Returns a list of random weapons (in WeaponData)
func get_random_weapons(amount: int, level: int, exceptions: Array[WeaponData]) -> Array[WeaponData]:
	var weapon_options: Array[String] = _get_weapon_options()
	for weapon_data in exceptions:
		weapon_options.erase(weapon_data.name)
	
	var result: Array[WeaponData] = []
	for i in amount:
		var random_name = weapon_options.pick_random()
		result.append(get_weapon_data(random_name, level))
		weapon_options.erase(random_name)
	
	return result

func _get_weapon_options() -> Array[String]:
	return ["axe", "dagger", "greatsword", "hammer", "longsword", "scythe", "spear", "staff", "star"]
