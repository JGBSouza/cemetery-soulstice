class_name WeaponCard
extends PanelContainer
## Weapon button (in weapons menu) code. 

var cost: int
var weapon_data: WeaponData
signal choosen_card(card: WeaponCard)

# Emitted when this card is selected, monitored by WeaponsMenu
func _on_button_pressed():
	choosen_card.emit(self)

# Updates all weapon card attributes to match the given weapon and cost
func set_weapon(weapon_data_: WeaponData, cost_: int):
	cost = cost_
	$Attributes/VBoxContainer/SoulsCounter/Counter.text = str(cost)
	weapon_data = weapon_data_
	$Attributes/VBoxContainer/Name.text = weapon_data.prefix + "\n" + weapon_data.name
	$Attributes/VBoxContainer/Level.text = "*".repeat(weapon_data.level)
	var atlas_texture = AtlasTexture.new()
	atlas_texture.atlas = weapon_data.atlas
	atlas_texture.region = \
	Rect2(weapon_data.sprite_size * weapon_data.sprite_index, weapon_data.sprite_size)
	$Attributes/VBoxContainer/WeaponTexture.texture = atlas_texture
	$Attributes/VBoxContainer/WeaponTexture.set_custom_minimum_size(Vector2(150,150))
	$Attributes/VBoxContainer/WeaponTexture.update_minimum_size()

# Disables the button interaction
func disable():
	modulate = Color(0.5, 0.5, 0.5, 1)
	$TextureButton.disabled = true
	$TextureButton.mouse_default_cursor_shape = Input.CURSOR_ARROW

# Enables the button interaction
func enable():
	modulate = Color(1, 1, 1, 1)
	$TextureButton.disabled = false
	$TextureButton.mouse_default_cursor_shape = Input.CURSOR_POINTING_HAND
