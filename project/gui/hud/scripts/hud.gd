extends Interface
## HUD visual interface code.

var health: int = 0
var max_health: int = 1
var horde: int = 0
var souls: int = 0

# Called when the object is created 
func _ready():
	# Try to get player reference, connecting all status change signals
	if get_tree().has_group("player"):
		var player: Player = get_tree().get_nodes_in_group("player")[0]
		$VBoxContainer/ProgressBar.max_value = player.get_max_health()
		change_health(player.get_current_health())
		change_souls(player.get_current_souls())
		player.get_health_signal().connect(change_health)
		player.get_souls_signal().connect(change_souls)
	# Try to get level reference
	if get_tree().has_group("level"):
		var level: Level = get_tree().get_nodes_in_group("level")[0]
		# Connect horde increase signal
		level.next_horde.connect(increase_horde)

func display():
	self.show()

func close():
	interface_closed.emit()
	self.hide()

# Change HUD current health value
func change_health(amount: int):
	$VBoxContainer/ProgressBar.value += amount

# Change HUD current souls value
func change_souls(amount: int):
	souls += amount
	$VBoxContainer/SoulsCounter/Counter.text = str(souls)

# Increases HUD current horde value
func increase_horde():
	horde += 1
	$VBoxContainer/Horde.text = "Horde " + str(horde)
