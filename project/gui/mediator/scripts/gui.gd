class_name GUI
extends CanvasLayer
## GUI mediator node. Handles all GUI related requests.
## Interfaces are handled in stack logic, displaying one "on top" of the other (but 
## only one can be seen by the player at the time). Once the top window is closed, 
## it will display the previously hidden interface, using the linked list to do so.

enum Interfaces { HUD, WEAPONS_MENU, PAUSE } # All possible menus/interfaces to display
@onready var _interface_nodes = {
	Interfaces.HUD : $HUD,
	Interfaces.WEAPONS_MENU : $WeaponsMenu,
	Interfaces.PAUSE : $Pause
}
var top: LinkedList = null # Linked list top

# Called when the object is created 
func _ready() -> void:
	display(GUI.Interfaces.HUD)

# Display a new interface/menu. Expects one of the Interfaces enum options as argument.
func display(interface: Interfaces) -> Signal:
	var interface_node: Interface = _interface_nodes[interface]
	if top:
		if top.interface == interface_node:
			return interface_node.interface_closed
		top.interface.hide()
	top = LinkedList.new(top, interface_node) # New linked list node
	top.interface.display()
	top.interface.connect("interface_closed", _on_interface_closed)
	# Returns the recently displayed interface "interface_closed" signal in case 
	# the caller wants to receive this notification
	return interface_node.interface_closed

# Called when the top interface closes.
func _on_interface_closed() -> void:
	top.interface.disconnect("interface_closed", _on_interface_closed)
	top = top.previous
	if top:
		top.interface.display()

# Linked list class. It's used to keep track of the "interfaces stack"
class LinkedList:
	var interface: Interface = null
	var previous: LinkedList = null
	
	func _init(arg_previous: LinkedList, arg_interface: Interface):
		previous = arg_previous
		interface = arg_interface
