class_name Interface
extends Control
## Interface abstract class. Used to implement concrete visual interfaces.

signal interface_closed

func display() -> void:
	pass

func close() -> void:
	pass
