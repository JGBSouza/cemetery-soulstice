extends GutTest
# Code for Map Generation test.

var Map = preload('res://map/scenes/map.tscn')
var _map = null

func before_each():
	_map = autofree(Map.instantiate())


func after_each():
	_map.queue_free()


func test_put_random_thing_function():
	var x_position = [30, 30, 70, 70]
	var y_position = [30, 70, 30, 70]
	var x_wrong_position = [29, 80, 30, 70]
	var y_wrong_position = [30, 70, 20, 90]
	
	for i in range(4):
		var correct_cell := Vector2i(x_position[i], y_position[i])
		var wrong_cell := Vector2i(x_wrong_position[i], y_wrong_position[i])
		var correct_result = _map._on_gravestone_area(correct_cell)
		var wrong_result = _map._on_gravestone_area(wrong_cell)
		assert_eq(correct_result, true, "This cell should be in the gravestone area")
		assert_eq(wrong_result, false, "This cell should not be in the gravestone area")


func test_add_source_random_scene():
	var sources = [_map.Sources.GRAVESTONES, _map.Sources.OTHER_OBJECTS, _map.Sources.PLANTS, _map.Sources.STRUCTURES]
	var cells = [Vector2i(10,20), Vector2i(20,30), Vector2i(30,40), Vector2i(40,50)]
	
	for i in range(4):
		_map._add_source_random_scene(cells[i], _map.Layers.OBJECTS, sources[i])
		
		var result = _map.get_cell_source_id(_map.Layers.OBJECTS, cells[i])
		assert_eq(result, sources[i], "This cell should be another structure")


func test_add_source_random_tile():
	var sources = [_map.Sources.GRASS_TERRAIN, _map.Sources.STONE_TERRAIN]
	var cells = [Vector2i(20,40), Vector2i(30,60)]
	
	for i in range(2):
		_map._add_source_random_tile(cells[i], _map.Layers.TERRAIN, sources[i])
		
		var result = _map.get_cell_source_id(_map.Layers.TERRAIN, cells[i])
		assert_eq(result, sources[i], "This cell should be another terrains")




