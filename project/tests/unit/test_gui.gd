extends GutTest
# Code for GUI test.

var GUI = preload('res://gui/mediator/scenes/gui.tscn')
var _gui = null

func before_each():
	_gui = autofree(GUI.instantiate())
	_gui._ready()


func after_each():
	_gui.queue_free()


func test_display_showing():
	var interfaces = [_gui.Interfaces.WEAPONS_MENU, _gui.Interfaces.PAUSE, _gui.Interfaces.HUD]
	
	for i in range(2):
		var result_node = _gui.display(interfaces[i+1]).get_object()
		assert_eq(result_node.visible, true, "Node should be visible")
