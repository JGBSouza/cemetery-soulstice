extends GutTest
# Code for death interactivity test.

var Interactivity = preload('res://death/scenes/interactivity.tscn')
var _interactivity = null

var Player = preload('res://player/scenes/player.tscn')
var _player = null

func before_each():
	_interactivity = autofree(Interactivity.instantiate())
	_interactivity._ready()
	_player = autofree(Player.instantiate())


func after_each():
	_interactivity.queue_free()
	_player.queue_free()


func test_body_interaction():
	var signals = ["body_entered", "body_exited"]
	var results = [true, false]
	
	for i in range(2):
		_interactivity._interactable_area.emit_signal(signals[i], _player)
		var result = _interactivity._clickable_area.input_pickable
		
		assert_eq(result, results[i], "Interaction should be allowed")


func test_click_interaction():
	var event = autofree(InputEventMouseButton.new())
	event.pressed = true
	#_interactivity._clickable_area.emit_signal("input_event")
	# needs fixing
	var result = _interactivity._clickable_area.input_pickable
	
	assert_eq(result, false, "Input pickable should be false")
