extends GutTest
# Code for class_name ValueNumber test.

func test_get_value():
	var value_test = autofree(ValueNumber.new())
	assert_not_null(value_test.get_value()) 


func test_initial_value():
	var value_test = autofree(ValueNumber.new())
	assert_eq(0, value_test.get_value(), "Value should be initialized in 0")


func test_change_with_positive_values():
	var value_test = autofree(ValueNumber.new())
	var initial_value = value_test.get_value()
	value_test.change(100)
	assert_eq(initial_value + 100, value_test.get_value(), "Value should have changed to 100")


func test_change_with_negative_values():
	var value_test = autofree(ValueNumber.new())
	var initial_value = value_test.get_value()
	value_test.change(-100)
	assert_eq(initial_value - 100, value_test.get_value(), "Value should have change to -100")


func test_max_value_limit():
	var value_test = autofree(ValueNumber.new())
	value_test.change(100000)
	assert_eq(1000, value_test.get_value(), "Value should not be greater than the max limit")
