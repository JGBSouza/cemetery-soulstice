extends GutTest
# Code for TimeOfDay test.

var TimeOfDay = preload('res://time_of_day/scenes/time_of_day.tscn')
var _time_of_day = null

func before_each():
	_time_of_day = autofree(TimeOfDay.instantiate())
	_time_of_day._ready()


func test_get_day_duration():
	var duration = _time_of_day.get_day_duration()
	assert_not_null(duration)


func test_get_night_duration():
	var duration = _time_of_day.get_night_duration()
	assert_not_null(duration)


func test_get_time_progress():
	var duration = _time_of_day.get_time_progress()
	assert_eq(duration, 1.0, "Time progress should be at the start")
