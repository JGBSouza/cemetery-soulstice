extends GutTest
# Code for script level.gd

func test_get_horde():
	var level = autofree(Level.new())
	var first_horde = level.get_horde()
	assert_not_null(first_horde, "Get horde returned null")


func test_initial_horde():
	var level = autofree(Level.new())
	var first_horde = level.get_horde()
	assert_eq(1, first_horde, "First horde is different than 1")


func test_next_horde():
	var level = autofree(Level.new())
	var first_horde = level.get_horde()
	level._next_horde()
	assert_eq(first_horde + 1, level.get_horde(), "First horde is different than 1")


func test_max_horde_number():
	var level = autofree(Level.new())
	for i in range(30):
		level._next_horde()
	var max_horde = level.get_horde()
	level._next_horde()
	assert_eq(max_horde, level.get_horde(), "First horde is different than 1")
