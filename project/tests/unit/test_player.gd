extends GutTest
# Code for Player test.

var Player = preload('res://player/scenes/player.tscn')
var _player = null
var _sender = InputSender.new(Input)

func before_each():
	_player = autofree(Player.instantiate())
	_player._ready()


func test_getting_velocity_back():
	_player._set_player_velocity(200)
	var result = _player._move_speed
	
	assert_eq(result, 65.0, "Result should have been the default velocity")


func test_animation_tree_initialized_correctly():
	var result = _player._animation_tree
	assert_not_null(result);


func test_capture_move():
	var move_array_x = [-1, 1, 0, 0]
	var move_array_y = [0, 0, -1 ,1]
	var action_array = ["ui_left", "ui_right", "ui_up", "ui_down"]
	
	for i in range(4):
		_player._move()
		_sender.action_down(action_array[i]).hold_for(1)
		await _sender.idle
		
		var result = _player._capture_player_movements()
		assert_eq(result, Vector2(move_array_x[i], move_array_y[i]), "Incorrect result for test_capture_move")


func test_undashing_function():
	_player.dashing = true
	assert_eq(_player.dashing, true, "Player dashing should be equal to true")
	
	_player._on_animation_tree_animation_finished("dash")
	assert_eq(_player.dashing, false, "Player dashing should be equal to false")

func test_take_damage():
	assert_false(_player.is_dead, 'Player should be alive after being created')
	
	_player.take_damage(2000)
	assert_true(_player.is_dead, 'Player should be alive after being created')


func test_restore_alived_state():
	_player.take_damage(2000)
	_player.restore_health()
	assert_true(_player.is_dead, 'Player should be alive after restored health')


func test_get_current_health():
	var initial_health = _player.get_current_health()
	assert_not_null(initial_health)


func test_get_max_health():
	var max_health = _player.get_max_health()
	assert_gt(max_health, 0, "Max health should be greater than 0")


func test_restore_health():
	var max_health = _player.get_max_health()
	_player.take_damage(20)
	_player.restore_health()
	assert_eq(_player.get_current_health(), max_health, "Player dashing should be equal to false")


func test_get_current_souls():
	var initial_souls = _player.get_current_souls()
	assert_not_null(initial_souls)


func test_change_number_of_souls():
	var initial_souls = _player.get_current_souls()
	_player.change_souls(20)
	
	var current_souls = _player.get_current_souls()
	assert_ne(initial_souls, current_souls, "Current number of souls should be different from the initial number")
	assert_eq(current_souls, initial_souls + 20, "Current number shoubel be initial number + 20")


func test_get_current_weapon():
	var initial_weapon = _player.get_current_weapon()
	assert_not_null(initial_weapon, "First weapon should not be null")


func test_change_weapon():
	var initial_weapon = _player.get_current_weapon()
	var weapon_example = autofree(WeaponData.new())
	_player.change_weapon(weapon_example)
	var second_weapon = _player.get_current_weapon()
	assert_ne(initial_weapon, second_weapon, "Weapons should have been changed")
