class_name Player
extends CharacterBody2D
## This script handles the player.
## It's responsible to get inputs from the player and make the
## player character react accordingly.

signal dead

# Define and export class variables to Godot Editor 
@export_category("Objects")
@export var _animation_tree: AnimationTree = null

@export_category("Variables")
@export var is_dead: bool = false
@export var _move_speed: float
@export var _dash_speed: float 
@export var _dash_cooldown : float
@export var _friction: float
@export var _acceleration: float

var _cooldown_timer: Timer
var _is_dash_in_cooldown : bool = false
var _state_machine
var _direction: Vector2 = Vector2.ZERO
var dashing: bool = false

# Process player movement
func _move() -> void:
	if !dashing:
		_direction = _capture_player_movements()
	
	# Check if player is moving
	if _direction != Vector2.ZERO:
		if Input.is_action_just_pressed("dash") and not _is_dash_in_cooldown:
			dashing = true
		_set_animation_tree()
		_set_player_velocity(_acceleration)
		return
	_set_player_velocity(_friction)

# Get movemente vector from player input
func _capture_player_movements():
	return Vector2(
		Input.get_axis("ui_left", "ui_right"), 
		Input.get_axis("ui_up", "ui_down")
	)

# Accelarate or decelerate player
func _set_player_velocity(_current_acceleration: float):
	var speed = _dash_speed if dashing else _move_speed
	velocity = lerp(velocity, _direction.normalized() * speed, _current_acceleration)

# Set player movement directon to define correct asset
func _set_animation_tree():
	_animation_tree["parameters/idle/blend_position"] = _direction
	
	# Set horizontal movement asset over vertical on digonal movement
	if _direction.x != 0:
		_animation_tree["parameters/walk/blend_position"] = Vector2(_direction.x, 0)
	else:
		_animation_tree["parameters/walk/blend_position"] = Vector2(0, _direction.y)
	
	# Do the same for dashing animation
	if _direction.x != 0:
		_animation_tree["parameters/dash/blend_position"] = Vector2(_direction.x, 0)
	else:
		_animation_tree["parameters/dash/blend_position"] = Vector2(0, _direction.y)

# Handle player asset changes
func _animate_player() -> void:
	if dashing:
		_state_machine.travel("dash")
	elif velocity.length() > 2:
		_state_machine.travel("walk")
	else:
		_state_machine.travel("idle")

# Called by the animation tree every time an animation finishes
func _on_animation_tree_animation_finished(anim_name: StringName) -> void:
	if dashing && anim_name.contains("dash"):
		dashing = false
		_is_dash_in_cooldown = true
		_cooldown_timer.start()

func take_damage(_damage: float) -> void:
	$Health.change(-_damage)

	$Animation.play("on_hit")
	if $Health.get_value() <= 0:
		is_dead = true
		dead.emit()

func _enable_dash() -> void:
	_is_dash_in_cooldown = false

## Player Getters

# Send signal to HUD when life attribute is changed
func get_health_signal() -> Signal:
	return $Health.changed

# Get current health value
func get_current_health():
	return $Health.get_value()

# Get max health value
func get_max_health():
	return $Health.max_value

# Send signal to HUD when souls attribute is changed
func get_souls_signal() -> Signal:
	return $Souls.changed
	
# Get current souls value
func get_current_souls():
	return $Souls.get_value()
# Get current weapon WeaponData object
func get_current_weapon() -> WeaponData:
	return $Weapon.get_weapon_data()
	
func restore_health() -> void:
	$Health.change($Health.max_value)

# Add souls when the players kills monsters
func change_souls(amount: int) -> void:
	$Souls.change(amount)

# Change weapon to given WeaponData
func change_weapon(weapon_data: WeaponData) -> void:
	$Weapon.change_weapon(weapon_data)

# Setup dash cooldown timer
func _set_up_dash_cooldown_timer() -> void:
	_cooldown_timer = Timer.new()
	_cooldown_timer.wait_time = _dash_cooldown
	_cooldown_timer.connect("timeout", _enable_dash)
	add_child(_cooldown_timer)

# Setup collision layer and mask and motion mode
func _set_up_collision() -> void:
	set_collision_layer_value(1, true)
	set_collision_mask_value(2, true)
	set_motion_mode(CharacterBody2D.MOTION_MODE_FLOATING)

# Handle initial variables processing
func _ready():
	_set_up_dash_cooldown_timer()
	_state_machine = _animation_tree["parameters/playback"]
	_set_up_collision()

# Handle player looping analysis
func _physics_process(_delta: float) -> void:
	if is_dead:
		return
	_move()
	_animate_player()
	move_and_slide()
