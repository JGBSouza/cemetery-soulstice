class_name ValueNumber
extends Node

@export var value: int = 0
@export var max_value: int = 1000

signal changed(amount: int)

func get_value():
	return value

func change(other: int):
	value += other
	if(value > max_value):
		value = max_value
	changed.emit(other)
