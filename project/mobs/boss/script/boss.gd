extends Mob
# Code for implementation of Mob 'Boss'
class_name Boss

signal killed

# Set attack strategy variables
@export var _attack_switcher_cooldown : int
@export var _dash_speed : int

# Boss Getters

# Return dash_speed
func get_dash_speed() -> int:
	return _dash_speed

# Return attack_switcher cooldown
func get_attack_switcher_cooldown():
	return _attack_switcher_cooldown

# Instantiate super class and own attack strategy
func _init():
	super._init()
	_attack_strategy = BossAttack.new(self)

# After death animation emits killed signal
func _emit_killed_signal_on_death_animation_end(_animationName : StringName):
	killed.emit()
