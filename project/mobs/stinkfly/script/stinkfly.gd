extends Mob
# Code for implementation of Mob 'StinkFly'

class_name Stinkfly

# Set attack strategy variables
var _projectile: Projectile = StinkflyProjectile.new()
var _casting_time : float = 1.0

# Getters
func get_casting_time() -> float:
	return _casting_time
func get_projectile() -> Projectile:
	return _projectile

# Initialize mob and corresponding attack strategy
func _init():
	super._init()
	_attack_strategy = RangedAttack.new(self)
	
