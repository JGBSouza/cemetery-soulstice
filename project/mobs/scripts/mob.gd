class_name Mob
# Code for implementation of Class Mob

extends CharacterBody2D

# Define and export class variables to Godot Editor 
@export_category("Objects")
@export var _animation_tree: AnimationTree = null

@export_category("Variables")
@export var _move_speed: float = 20.0 
@export var _target : Node2D 
var _state_machine

#combat related variables
@export var _life: float 
@export var _damage: float 
@export var _attack_cooldown: float
@export var _attack_range : float 
var _is_dead: bool 
var _attacking: bool 
var _cooldown_timer: Timer
var _attack_in_cooldown: bool 
var _attack_strategy : AttackStrategy


# movement related variables
var _direction : Vector2

@onready var _navigation_agent : NavigationAgent2D = $NavigationAgent2D 

## Mob Getters

# Return mobs damage
func get_damage() -> float:
	return _damage

# Return mobs global position
func get_current_position() -> Vector2:
	return global_position

# Return mobs target global position
func get_target_position() -> Vector2:
	return _target.global_position

# Return mob attack range
func get_attack_range() -> float:
	return _attack_range

# Set up attack cooldown timer that blocks mob attack
func _set_up_attack_cooldown_timer() -> void:
	_cooldown_timer = Timer.new()
	_cooldown_timer.wait_time = _attack_cooldown
	_cooldown_timer.connect("timeout", _enable_attack)
	add_child(_cooldown_timer)

# Set up navigation related variables 
func _set_up_navigation() -> void:
	_navigation_agent.path_desired_distance = 0.8 * _attack_range 
	_navigation_agent.target_desired_distance = 0.8 * _attack_range
	
# Set up player collision layer and mask and motion mode
func _set_up_collision() -> void:
	set_collision_layer_value(2, true)
	set_collision_mask_value(1, true)
	set_motion_mode(CharacterBody2D.MOTION_MODE_FLOATING)

# Enable player attack after cooldown ends
func _enable_attack() -> void:
	_attack_in_cooldown = false

# Check if mob is on his range attack and attack not in cooldown
func _is_able_to_attack() -> bool:
	return	((get_current_position() - get_target_position()).length() <= _attack_range and 
			 not _attack_in_cooldown)

# Defines mob Target
func _actor_setup() -> void:
	await get_tree().physics_frame 
	
	if get_tree().has_group("player"):
		_target = get_tree().get_nodes_in_group("player")[0]
	else: 
		printerr("Target Player is not defined")

# Process movement
func _move() -> void:
	if _target == null or _attacking or _is_dead:
		return
		
	if _target.is_dead:
		velocity = Vector2.ZERO
		return
	
	_set_target()
	
	if _is_able_to_attack():
		_attack_strategy.attack()
		_attacking = true
		return
	
	if _navigation_agent.is_navigation_finished():
		velocity = Vector2.ZERO
		return
		
	var next_path_position : Vector2
	
	next_path_position = _navigation_agent.get_next_path_position()
	_direction = (next_path_position - get_current_position()).normalized()
	
	if _direction != Vector2.ZERO:
		_set_animation_tree()
		velocity = _direction * _move_speed

#Set mob movement directon to define correct asset
func _set_animation_tree() -> void:
	var x_direction : Vector2 = Vector2(_direction.x, 0)
	_animation_tree["parameters/idle/blend_position"] = x_direction
	_animation_tree["parameters/walk/blend_position"] = x_direction

# Handle character asset changes
func _animate_character() -> void:
	if _attacking:
		_state_machine.travel("attack")
	elif velocity != Vector2.ZERO:
		_state_machine.travel("walk")
	else:
		_state_machine.travel("idle")
	
func _set_target() -> void:
	_navigation_agent.target_position = get_target_position()

func take_damage(damage: float) -> void:
	_life -= damage
	$AnimationPlayer.play("on_hit")
	if _life <= 0.0:
		_target.change_souls(1)
		die()

func die() -> void:
	_is_dead = true
	_state_machine.travel("death")

func _on_animation_tree_animation_finished(animation_name: StringName) -> void:
	if _is_dead && animation_name.contains("death"):
		queue_free()
	if _attacking && animation_name.contains("attack"):
		_attacking = false
		_attack_in_cooldown = true
		_cooldown_timer.start()

# Handle mob looping analysis
func _physics_process(_delta: float) -> void:
	if _is_dead:
		return
	_move()
	_animate_character()
	move_and_slide()

func _ready():
	_state_machine = _animation_tree["parameters/playback"]		
	_set_up_attack_cooldown_timer()
	_set_up_navigation()
	_set_up_collision()
	add_child(_attack_strategy)
	call_deferred("_actor_setup")
