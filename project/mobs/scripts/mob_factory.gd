extends Node
class_name MobFactory

# Dictionary for mob name and corresponding PackedScene
var mob_dictionary : Dictionary

# Initialize variables
func _init():
	var bloater_scene : PackedScene = load("res://mobs/bloater/scene/bloater.tscn")
	var stinkfly_scene : PackedScene = load("res://mobs/stinkfly/scene/stinkfly.tscn")
	var scratcher_scene : PackedScene = load("res://mobs/scratcher/scene/scratcher.tscn")
	var fire_skull_scene : PackedScene = load("res://mobs/fire_skull/scene/fire_skull.tscn")
	var goblin_scene : PackedScene = load("res://mobs/goblin/scene/goblin.tscn")
	var boss_scene : PackedScene = load("res://mobs/boss/scene/boss.tscn")
	
	mob_dictionary = {
		"Bloater" : bloater_scene,
		"Stinkfly" : stinkfly_scene,
		"Scratcher" : scratcher_scene,
		"FireSkull" : fire_skull_scene,
		"Goblin" : goblin_scene,
		"Boss" : boss_scene
	}

# Create and return mob scene
func create(mob_name : String) -> Mob:
	var mob : PackedScene = mob_dictionary[mob_name]
	return mob.instantiate()
