class_name Goblin
# Code for implementation of Mob 'Goblin'

extends Mob

@export var _dash_speed : int

# Goblin getter
func get_dash_speed() -> int:
	return _dash_speed

# Initialize super function and set attack strategy
func _init():
	super._init()
	_attack_strategy = DashAttack.new(self)

	
