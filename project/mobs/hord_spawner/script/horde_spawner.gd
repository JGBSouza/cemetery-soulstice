extends Node2D
class_name HordSpawner

@export var _horde_size: Array[int]
@export var monster_probability_dict : Dictionary

var _monster_factory : MobFactory
var _horde_level : int
var _mobs_spawned : int

func _ready():
	_monster_factory = MobFactory.new()
	_set_signals()

# Set up spawn and hord kill respectively when night and day starts 
func _set_signals() -> void:
	if get_tree().has_group("timeofday"):
		var time_of_day: TimeOfDay = get_tree().get_nodes_in_group("timeofday")[0]
		time_of_day.night_start.connect(_spawn_horde)
		time_of_day.day_start.connect(_kill_remaining_horde)

# Spawn horde at a random place in the map 
func _spawn_horde() -> void:
	_set_up_horde()

	while _mobs_spawned <= _horde_size[_horde_level]:
		var monster_kind = _sort_monster()
		var mob : Mob = _monster_factory.create(monster_kind)
		var cell : Vector2i = _get_random_cell()
		_put_mob(mob, cell)
		_mobs_spawned += 1

# Get random cell to spawn the mob
func _get_random_cell() -> Vector2i:
	var map : TileMap = get_parent().get_node("Map")
	var map_size : int = map.get_map_size()
	var structures_position : Array[Vector2i] = map.get_structures_positions()
	
	# Range 1 to map_size -1 to evoid mobs colliding with the map border
	var rand_i : int = randi_range(1, map_size-1) 
	var rand_j : int = randi_range(1, map_size-1)
	var cell := Vector2i(rand_i, rand_j)
	if cell in structures_position:
		return _get_random_cell()
	return Vector2i(rand_i, rand_j)
	
# Setup new hord status
func _set_up_horde() -> void:
	_mobs_spawned = 0 
	_horde_level = get_parent().get_horde() - 1
	if(_horde_level == 9):
		_spawn_boss()

func _spawn_boss() -> void:
	var mob : Mob = _monster_factory.create("Boss")
	var cell : Vector2i = _get_random_cell()
	_put_mob(mob, cell)
	
# Put a mob in a given cell
func _put_mob(mob: Mob, cell : Vector2i) -> void:
	var TILE_SIZE = get_parent().get_node("Map").get_tile_set_size()
	mob.position = cell * TILE_SIZE
	get_parent().add_child.call_deferred(mob)

# Sort a random monster to instanciate based on the hord spawn probability
func _sort_monster() -> String:
	var rand = randf()
	var soma_probabilidades = 0.0
	var monsters_kinds : Array = monster_probability_dict.keys()
	
	for kind in monsters_kinds:
		soma_probabilidades += monster_probability_dict[kind][_horde_level]
		if rand <= soma_probabilidades:
			return kind
			
	return monsters_kinds[-1]

# Kill remaining level children that are mobs after day starts
func _kill_remaining_horde() -> void:
	var children_nodes = get_parent().get_children()
	for children in children_nodes:
		if children is Mob:
			children.die()
		if children is Projectile:
			children.queue_free()
			
