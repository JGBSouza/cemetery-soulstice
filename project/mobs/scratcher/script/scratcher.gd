extends Mob
# Code for implementation of Mob 'Scratcher'

class_name Scratcher

# Initialize mob and corresponding attack strategy
func _init():
	super._init()
	_attack_strategy = MeleeAttack.new(self)
