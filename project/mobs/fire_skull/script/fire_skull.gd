extends Mob
# Code for implementation of Mob 'Fire Skull'

class_name FireSkull

# Set attack strategy variables
var _projectile: Projectile = FireSkullProjectile.new()
var _casting_time : float = 0.2

# Mob Getters
func get_casting_time() -> float:
	return _casting_time
func get_projectile() -> Projectile:
	return _projectile

# Initialize mob and corresponding attack strategy
func _init():
	super._init()
	_attack_strategy = RangedAttack.new(self)
