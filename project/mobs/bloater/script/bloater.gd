extends Mob
# Code for implementation of Mob 'Bloater'

class_name Bloater

# Initialize mob and corresponding attack strategy
func _init():
	super._init()
	_attack_strategy = MeleeAttack.new(self)

