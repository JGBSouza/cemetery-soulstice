extends StaticBody2D
## Death character script. Handles the concrete interaction implementation
## (plays character animation, communicates with GUI, etc.)

@onready var _animation_player := $AnimationPlayer
var _gui: GUI
var _player: Player

# Called when the object is created 
func _ready() -> void:
	# Try to get GUI singleton reference
	if get_tree().has_group("GUI"):
		_gui = get_tree().get_nodes_in_group("GUI")[0]
	# Try to get level reference
	if get_tree().has_group("level"):
		var level: Level = get_tree().get_nodes_in_group("level")[0]
		# Connect day time related signals
		level.get_day_signal().connect(_start_day)
		level.get_night_signal().connect(_start_night)
	# Try to get player reference
	if get_tree().has_group("player"):
		_player = get_tree().get_nodes_in_group("player")[0]

# Disables interaction on night time
func _start_night() -> void:
	$Interactivity.disable()

# Enables interaction on day time
func _start_day() -> void:
	$Interactivity.enable()

# Implements the result of interacting with this character
func interact() -> void:
	_player.restore_health()
	_animation_player.play("activate")
	await _animation_player.animation_finished
	# Ask GUI to show weapons menu and wait for it to close
	var interface_closed: Signal = _gui.display(GUI.Interfaces.WEAPONS_MENU)
	await interface_closed
	_animation_player.play("deactivate")
	$Interactivity.disable()
