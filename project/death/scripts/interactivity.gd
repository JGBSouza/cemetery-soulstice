extends Node2D
## Interaction component script. Handles mouse input interaction and calls 
## "interactable" functions. The player will only be able to click on the 
## clickable area when they are within the interactable area.

@export var interactable: Node # Who will implement the interaction result

var enabled: bool = true

@onready var _interactable_area := $InteractableArea
@onready var _clickable_area := $ClickableArea
@onready var _connections := {
	_interactable_area : {
		"body_entered": _on_body_entered,
		"body_exited": _on_body_exited,
	},
	_clickable_area : {
		"mouse_entered": _on_mouse_entered,
		"mouse_exited": _on_mouse_exited,
		"input_event": _on_input_event,
	},
}

# Called when the object is created 
func _ready():
	# Setup various node signal connections
	for connectable in _connections:
		var pairs: Dictionary = _connections[connectable]
		for signal_name in pairs:
			var callable: Callable = pairs[signal_name]
			connectable.connect(signal_name, callable)

func enable():
	enabled = true

func disable():
	enabled = false

# Called when a body enters the interactable area
func _on_body_entered(body):
	if body is Player and enabled:
		# Enables the clickable area monitoring
		_clickable_area.input_pickable = true

# Called when a body leaves the interactable area
func _on_body_exited(body):
	if body is Player:
		# Disables the clickable area monitoring
		_clickable_area.input_pickable = false

# Called when the mouse enters the clickable area
func _on_mouse_entered():
	Input.set_default_cursor_shape(Input.CURSOR_POINTING_HAND)

# Called when the mouse leaves the clickable area
func _on_mouse_exited():
	Input.set_default_cursor_shape(Input.CURSOR_ARROW)

# Called when an input event involving the clickable area happens (hopefully a click)
func _on_input_event(_viewport, event, _shape_idx):
	if (event is InputEventMouseButton and event.pressed):
		if interactable.has_method("interact"):
			interactable.interact()
		_clickable_area.input_pickable = false
