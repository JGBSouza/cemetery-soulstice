@tool # Executes the script in the editor
extends Sprite2D
class_name Shadow

## This script automatically adds a dropshadow effect that reacts to time
## of day changes.
## The parent of the node holding this script should be the sprite you
## want to add a dropshadow to.
## This script should be used by adding the shadow.tscn scene as a child
## of a sprite node.

var parent: Sprite2D


# Called when the object is created
func _ready():
	# The parent node of the shadow should be a Sprite2D that you want to add
	# a shadow to
	parent = self.get_parent() as Sprite2D
	if parent == null: return
	# Set the shadow's texture to be the same as the parent's
	self.texture = parent.texture
	# Copy sprite region data from the parent
	self.region_enabled = parent.region_enabled
	self.region_rect = parent.region_rect
	# Copy the parent's offset
	self.offset = parent.offset


# Called every frame
func _process(_delta):
	# Copy parent's animation data
	self.hframes = parent.hframes
	self.vframes = parent.vframes
	self.frame = parent.frame
	self.frame_coords = parent.frame_coords
	self.flip_h = parent.flip_h
