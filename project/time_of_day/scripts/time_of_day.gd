extends Node2D
class_name TimeOfDay
## This script is responsible for keeping track and handling the
## day and night cycle.
## It keeps track of the current time, if it's day or night, modifies
## the lighting of the environment and updates the shadows to make
## them reactive to changes in the time of day.

signal night_start                   # Emitted when the night begins
signal day_start                     # Emitted when the day begins

@export var duration_day: float      # Duration in seconds of day time
@export var duration_night: float    # Duration in seconds of night time
@export var gradient_day: Gradient   # Color gradient for day time
@export var gradient_night: Gradient # Color gradient for night time
@export var curve_day: Curve         # Intensity curve of the light in day time
@export var curve_night: Curve       # Intensity curve of the light in night time

# Called when the object is created
func _ready():
	$DayTimer.start(duration_day)

func get_day_duration():
	return duration_day

func get_night_duration():
	return duration_night

# Called once per frame
func _process(_delta):
	if $DayTimer.is_stopped() and $NightTimer.is_stopped():
		return
	var time_progress := get_time_progress()
	
	# Iterate over all shadow objects on the scene
	for shadow in self.get_tree().get_nodes_in_group("shadow"):
		shadow = shadow as Sprite2D # This is just for intellisense
		if shadow == null: continue
		# The skew of the shadow sprite goes from [-65, 65] based on the time
		shadow.skew = deg_to_rad(-(time_progress * 130 - 65))
		# The transparency of the shadow sprite goes from 0 to 0.7 to 0 again
		# based on the time
		shadow.modulate.a = (1 - abs((time_progress - 0.5) * 2)) * 0.5
	
	# Set the current energy of the light
	$Light.energy = get_energy_at(time_progress)
	# Set the current color of the light
	$Light.color = get_color_at(time_progress)

# Returns the current time
func get_current_time() -> float:
	if not $DayTimer.is_stopped():
		return duration_day - $DayTimer.time_left # Day
	return duration_night - $NightTimer.time_left # Night

# Returns the current time progress
func get_time_progress() -> float:
	if not $DayTimer.is_stopped():
		return (duration_day - $DayTimer.time_left) / duration_day   # Day
	return (duration_night - $NightTimer.time_left) / duration_night # Night

# Returns the energy of the light at a given time progress
func get_energy_at(progress: float) -> float:
	if not $DayTimer.is_stopped():
		return curve_day.sample(progress) # Day
	return curve_night.sample(progress)   # Night

# Returns the color of the light at a given time progress
func get_color_at(progress: float) -> Color:
	if not $DayTimer.is_stopped():
		return gradient_day.sample(progress) # Day
	return gradient_night.sample(progress)   # Night

# Callback: Called every time the day timer times out
func _on_day_timer_timeout():
	night_start.emit()
	# Start the night timer
	$NightTimer.start(duration_night)

# Callback: Called every time the night timer times out
func _on_night_timer_timeout():
	day_start.emit()
	# Start the day timer
	$DayTimer.start(duration_day)
