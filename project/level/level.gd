class_name Level
extends Node2D
## Handles all level magnitude communication and events.

signal next_horde

const MAX_HORDE: int = 10

@export_file("*.tscn") var death_screen_path
@export_file("*.tscn") var win_screen_path

var horde : int = 1
var _gui: GUI
var _time_of_day: TimeOfDay

# Called when the node enters the tree
func _enter_tree():
	_time_of_day = $TimeOfDay

# Called when the object is created
func _ready():
	_time_of_day = get_node("TimeOfDay")
	# Try to get GUI singleton reference
	if get_tree().has_group("GUI"):
		_gui = get_tree().get_nodes_in_group("GUI")[0]
	# Try to get player reference
	if get_tree().has_group("player"):
		var player: Player = get_tree().get_nodes_in_group("player")[0]
		player.dead.connect(_on_player_death)
	
	get_night_signal().connect(_next_horde)
	# Positions central objects in the actual center
	$Center.position = $Map.get_map_center()

func _unhandled_input(event):
	# Handles pause action
	if event.is_action_pressed("ui_cancel"):
		_gui.display(GUI.Interfaces.PAUSE)
		get_tree().paused = true

func get_horde() -> int:
	return horde

# Returns a start of day signal
func get_day_signal() -> Signal:
	return _time_of_day.day_start

# Returns a start of night signal
func get_night_signal() -> Signal:
	return _time_of_day.night_start

func get_night_duration() -> float:
	return _time_of_day.get_night_duration()

func _on_player_death() -> void:
	# Scene transition singleton
	Transition.transition(death_screen_path)

func _on_player_win() -> void:
	# Scene transition singleton
	Transition.transition(win_screen_path)

func _next_horde() -> void:
	if horde < MAX_HORDE:
		horde += 1
		next_horde.emit()

# When boss get instanciated stop Night timer and listen to the signal when he dies
func _on_child_entered_tree(child):
	if child is Boss:
		var boss: Boss = get_tree().get_nodes_in_group("boss")[0]
		boss.killed.connect(_on_player_win)
		_time_of_day.get_node("NightTimer").stop()
