# Cemetery Soulstice

## Integrantes
- Bruno Armond Braga
- Guilherme Vinicius Ferreira de Assis
- João Guilherme Barbosa de Souza
- Pedro Tonini Rosenberg Schneider

## Projeto
O projeto escolhido foi desenvolver um jogo utilizando a Godot Engine. Inspirado no arcade Robotron 2084 e mesclado com elementos de RPG (como Zelda), o jogo tem como desafio sobreviver a diversas hordas de inimigos enquanto oferece um sistema interessante de upgrade de armas.

O jogador é um coveiro que foi encarregado pela Morte de cuidar das almas penadas de um cemitério.

O jogo é composto por um loop entre noite e dia: 
- Durante a noite as almas (inimigos) estão a solta e o jogador deve ceifar o máximo que conseguir para ganhar pontos com a Morte. 
- Durante o dia a Morte aparece para recompensar o jogador pela noite anterior, oferecendo armas diferentes e possíveis upgrades (que custam o "score" de almas ceifadas). Nesse período o jogador deve se preparar para a noite seguinte escolhendo entre melhorar a arma atual ou trocar para uma arma nova. A melhoria da arma atual é mais cara que a troca de arma, mas as armas novas são do mesmo nível da atual. Assim, o jogador terá que decidir entre a flexibilidade barata ou a melhoria cara.

O jogo termina bem-sucedido caso o jogador consiga sobreviver por 10 noites.

## Como Rodar o Jogo

Para isso precisaremos ter a Godot Engine 4.1.2 instalada na sua máquina. Caso possua algum problema com esse pré-requisito, visite o [site oficial da game engine](https://godotengine.org/download/) para mais informações.

1 - Clone o repositório

2 - Abra a Godot Engine

3 - Clique na aba "Import" e escolha o arquivo com extensão .godot do nosso repositório

4 - Clique em "Import & Edit"

5 - Uma vez dentro da Game Engine, é possível rodar de duas maneiras.

5.1 - No canto superior direito, é possível usar a ferramente "Run Projet(F5)" para rodar o projeto como protótipo tendo a experiência de um jogador se o jogo já estivesse exportado.

5.2 - É possível também exportar o jogo direto, para isso deixamos a [documentação da Godot](https://docs.godotengine.org/pt_BR/4.x/tutorials/export/exporting_projects.html) te guiar, pois pode ter alguns passos. 

Se for de sua preferência também estamos deixando o jogo exportado na aba release, para rodar basta executar o arquivo.

## Cronograma
### Até a Fase 1:
- Melhorar o planejamento do jogo (definir regras ainda em aberto/com brechas, funcionamento geral das armas)
- Encontrar os assets necessários
- Implementar movimentação básica do jogador
- Implementar "gerador" de mapas
- Implementar ciclo de tempo

### Até a Fase 2:
- Melhorar o planejamento do jogo (definir tipos de inimigos, definir todas as armas, etc)
- Adicionar testes automatizados
- Implementar combate
- Implementar tipos de inimigos
- Implementar interação básica com a Morte

### Até a Fase 3:
- Implementar sistema de upgrades
- Implementar HUD
- Implementar menu
- Configurar hordas (ou "níveis")
- Testar e balancear o jogo
- Implementar troca de arma
- Correção de bugs encontrados na Fase 1

## Entregas
### Fase 1:
- Reunimos uma série de assets cruciais para dar vida ao jogo.
- Criamos um gerador de mapas aleatórios para assegurar que cada partida seja única (ainda possui alguns problemas, vide issues). 
- Estabelecemos um ciclo de tempo usando timers e adicionamos animações para fornecer feedback visual aos jogadores se é dia/noite e quanto tempo falta.
- Implementamos a movimentação básica do jogador, permitindo que os jogadores explorem o ambiente.
- Começamos a estudar como implementar testes automatizados no projeto, já que a Godot não oferece nenhuma ferramenta oficial para isso.

### Fase 2:
- Desenvolvemos um sistema de combate point-and-click para o jogador, completo com ações de ataque que seguem o mouse e habilidade de desviar de ataques de inimigos com um dash. As armas do jogador foram implementadas utilizando o padrão semelhante ao memento.
- Finalizamos a implementação inicial dos inimigos. Também foi iniciado o desenvolvimento dos comportamentos dos inimigos, como seguir o jogador e atacar.
- Adicionamos a personagem da morte no jogo, sendo possível o jogador interagir com ela e mostrar um menu protótipo onde será implementada a troca das diferentes armas do jogador.
- Implementamos o esqueleto da GUI utilizando o padrão singleton e mediator.
- Desenvolvemos testes automatizados para validar todas as features desenvolvidas nessa etapa e na etapa anterior.
- Reestruturamos algumas pastas do projeto para manter o padrão de organização mais consistente como sugerido no feedback anterior.

### Fase 3:
- Implementamos um sistema de upgrades de arma do personagem para dar uma dinâmica de progressão ao jogo.
- Implementamos a HUD de vida, sistema de almas e horda atual para dar feedback visual ao jogador de seus recursos.
- Implementamos um tutorial visual dos controles no começo do jogo.
- Implementamos os menus de início, pausa, derrota e vitória, utilizando um padrão mediator que serve para decidir qual das telas deve ser visualizada em cada momento.
- Configuramos o spawn da horda através da classe HordSpawner que utiliza o padrão de fábrica para instanciar os mobs quando necessário.
- Implementamos os projéteis disparados pelos mobs rangeds que utilizam padrão prototype para instanciar novas cópias de si mesmo.
- Implementamos as estratégias de ataque dos mob utilizando o padrão strategy uma vez que os mobs podem ter vários padrões de ataque (Ranged, Dash, Melee, Boss).
- Testamos e balanceamos os mobs e a evolução do sistema de armas do player, além da duração da noite e da quantidade de monstros spawnando em cada uma das hordas.
- Implementamos troca de arma que pode ser feita através da estátua da morte durante o dia.
- Implementamos um mob Boss que spawna na última fase.
- Aumentamos a cobertura de testes de classes existentes e criamos testes para as seguintes classes: TimeOfDay, ValueNumber e também para a cena Level.
- Corrigimos bugs encontrados nas fases 1 e 2.
- Corrigimos completamente a criação de orfãos nos testes.
- Centralizamos a morte no mapa.
- Fizemos várias mudanças nas armas para ficarem mais confortáveis de usar.

## Padrões do GoF utilizados:

1. Padrão Fábrica (Factory): Simplifica a criação de diferentes tipos de mobs em um jogo, mantendo o código organizado e fácil de modificar ao adicionar novos mobs.

2. Padrão Prototype: Permite criar projéteis e armas sem sobrecarregar o sistema, clonando instâncias existentes para economizar tempo e recursos.

3. Padrão Estratégia (Strategy): Ajuda a variar as estratégias de ataque dos mobs de forma flexível, separando os diferentes métodos de ataque em classes independentes.

4. Mediator: Usado para mediar a visualização de interfaces, centralizando as requisições numa classe só e resolvendo os problemas de concorrência de interface